import java.util.Scanner; 

public class Exercicio02 {

	public static void main(String[] args) 
	{

		float nota1, nota2, media;
		Scanner tecla = new Scanner(System.in);

		nota1 = 0;
		nota2 = 0;
		media = 0;

		System.out.println("Nota 1: ");
		nota1 = tecla.nextFloat();
		
		System.out.println("Nota 2: ");
		nota2 = tecla.nextFloat();		
		
		media = (nota1+nota2)/2;
		
		if (media>=6)
		{
			System.out.println("Parabéns");
		}
		
		else 
		{
			System.out.println("erro");
		}
		
	}

}
