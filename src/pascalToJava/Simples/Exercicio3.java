package pascalToJava.Simples;

import java.util.Scanner;

public class Exercicio3 {

	public static double ler, resultado;
	
	public static void main(String[] args)
	{
		
		@SuppressWarnings("resource")
		Scanner ler1 = new Scanner(System.in);

		System.out.println("Fahrenheit: ");
		ler = ler1.nextDouble();
		
		if(ler>0)
		{
			resultado = calcula(ler);
			System.out.println("Celsius: " + resultado);
		}
		
		else
		{
			System.out.println ("erro");
		}

	}
	
	public static double calcula (double valor)
	{
		return (valor-32)*5/9;		
	}
	
	

}
