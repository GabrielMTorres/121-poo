package pascalToJava.Simples;

import java.util.Scanner;

public class Exercicio1 {

	public static double ler, resultado;
	public final static double pi = 3.14;

	public static void main(String[] args)
	{
		
		@SuppressWarnings("resource")
		Scanner ler1 = new Scanner(System.in);

		System.out.println("Raio: ");
		ler = ler1.nextDouble();
		
		if(ler>0)
		{
			resultado = calcula(ler);
			System.out.println(resultado);
		}
		
		else
		{
			System.out.println ("erro");
		}

	}
	
	public static double calcula (double raio)
	{
		return pi*Math.pow(raio, 2);		
	}

}