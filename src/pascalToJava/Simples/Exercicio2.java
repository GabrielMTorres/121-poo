package pascalToJava.Simples;

import java.util.Scanner;

public class Exercicio2 {

	public static double ler, resultado;
	
	public static void main(String[] args)
	{
		
		@SuppressWarnings("resource")
		Scanner ler1 = new Scanner(System.in);

		System.out.println("Celsius: ");
		ler = ler1.nextDouble();
		
		if(ler>=0)
		{
			resultado = calcula(ler);
			System.out.println("Fahrenheit: " + resultado);
		}
		
		else
		{
			System.out.println ("erro");
		}

	}
	
	public static double calcula (double valor)
	{
		return (valor*9/5)+32;		
	}

}
